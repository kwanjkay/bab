"""Helper functions to return response data for success and error request"""
from rest_framework import status
from rest_framework.response import Response


def okay_response(response):
    """
    returns success response
    """
    data = {
        "status": "success",
        "data": response
    }
    return data


def error_response(error, message):
    """
    returns error response
    """
    data = {
        "status": "error",
        "error": error,
        "message": message
    }
    return data



def not_found_error_response(model_name):
    """
    returns 404 error response
    """
    data = {
        'status': 'error',
        'error': '{}_not_found.'.format(model_name),
        'message': 'Ensure the ID passed is of an existing {}.'.format(
            model_name)
    }
    return Response(data, status.HTTP_404_NOT_FOUND)


def no_records_error_response(model_name):
    """
    returns 404 error response
    """
    data = {
        'status': 'error',
        'error': '{}_not_found.'.format(model_name),
        'message': 'There are no {} in the database.'.format(model_name)
    }
    return Response(data, status.HTTP_404_NOT_FOUND)
