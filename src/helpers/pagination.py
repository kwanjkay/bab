"""Pagination helper."""

import math

from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework import status


class SetUserLimitOffsetPagination(LimitOffsetPagination):
    """Define pagination limit."""

    default_limit = 20

    def get_paginated_response(self, data):
        """Create and return paginated response."""
        page_number = math.ceil((self.offset + self.limit) / self.limit)
        return Response({
            "status": "success",
            "page_number": page_number,
            "total_items": self.count,
            "total_pages": math.ceil(self.count / self.limit),
            "links": {
                "next_page": self.get_next_link(),
                "previous_page": self.get_previous_link()
            },
            "data": data
        }, status=status.HTTP_200_OK)


class CustomPaginationMixin():
    """Define custom pagination."""

    @property
    def paginator(self):
        """The paginator instance associated with the view, or `None`."""
        if not hasattr(self, '_paginator'):
            if self.pagination_class is None:
                self._paginator = None
            else:
                self._paginator = self.pagination_class()
        return self._paginator

    def paginate_queryset(self, queryset):
        """Return a single page of results, `None` if pagination's disabled."""
        if self.paginator is None:
            return None
        return self.paginator.paginate_queryset(
            queryset, self.request, view=self)

    def get_paginated_response(self, data):
        """Return a paginated `Response` object given output data."""
        assert self.paginator is not None, 'paginator is None for CustomPaginationMixin'
        return self.paginator.get_paginated_response(data)
