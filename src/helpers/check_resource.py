"""
Helper functions for to query db
"""
from rest_framework import status

from src.helpers.save_serializer import save_serializer


def check_resource(model, model_serializer, pk, request, model_name):
    """
        returns a particular message for when resource exist or not.
    """
    try:
        resource = model.objects.get(pk=pk)
        if request.method == "GET":
            serializer = model_serializer(
                resource, context={'request': request})
            data = {
                'status': 'success',
                'data': serializer.data
            }
            return data, status.HTTP_200_OK
        serializer = model_serializer(
            resource, context={'request': request}, data=request.data,
            partial=True)
        if serializer.is_valid():
            data = save_serializer(serializer)
            return data, status.HTTP_200_OK
        data = {
            "status": "error",
            "error": "name is empty",
            "message": "Ensure the name is not empty"
        }
        return data, status.HTTP_400_BAD_REQUEST
    except model.DoesNotExist:
        data = {
            'status': 'error',
            'error': '{}_not_found.'.format(model_name),
            'message': 'Ensure the ID passed is of an existing {}.'.format(
                model_name)
        }
        return data, status.HTTP_404_NOT_FOUND


def get_obj_or_none(klass, *args, **kwargs):
    """
    This method will be used to check if object exists.
    If the object exists it returns the object else it returns None.
    query_p: The parameter to be used e.g id or username
    model: The model to query against eg User
    param: The value of parameter
    """
    try:
        res = klass.objects.get(*args, **kwargs)
    except:
        res = None
    return res


def filter_obj_or_none(klass, *args, **kwargs):
    try:
        res = klass.objects.filter(*args, **kwargs)
    except:
        res = None
    return res


def latest_obj_or_none(klass, *args, **kwargs):
    try:
        res = klass.objects.filter(*args, **kwargs).latest('created_at')
    except:
        res = None
    return res
