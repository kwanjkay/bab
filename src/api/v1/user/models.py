from django.db import models
from django.contrib.auth.models import AbstractUser

from src.common.models import AbstractBase


class UserStatus(AbstractBase):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    order = models.CharField(max_length=50)
    class Meta:
        db_table = "user_status"


class Role(AbstractBase):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    parameters = models.CharField(max_length=50)
    class Meta:
        db_table = "role"

class User(AbstractBase, AbstractUser):
    phone = models.CharField(max_length=20)
    email_verified = models.BooleanField(default=False)
    user_status = models.ForeignKey(
        UserStatus,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    role = models.ForeignKey(
        Role,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    class Meta:
        db_table = "user"

class BankingDetails(AbstractBase):
    account_types = (
        ('checking', 'Checking'),
        ('saving', 'Saving')
    )
    user = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    bank_name = models.CharField(max_length=50)
    bank_code = models.CharField(max_length=50)
    swift_code = models.CharField(max_length=50)
    proof_banking_details = models.CharField(max_length=250)
    is_active = models.BooleanField(default=True)
    account_type = models.CharField(
        choices=account_types,
        max_length=255, 
        null=True, blank=True
    )
    class Meta:
        db_table = "banking_details"
