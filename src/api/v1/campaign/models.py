from django.db import models
from django.contrib.postgres.fields import ArrayField
from src.api.v1.organization.models import Organization
from src.api.v1.user.models import User


# Create your models here.
from src.common.models import AbstractBase
from src.api.v1.currency.models import Currency

class CampaignCategory(AbstractBase):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "campaign_categories"

    def __str__(self):
        return f"{self.name}"


class CampaignSubCategory(AbstractBase):
    campaign_category_id = models.ForeignKey(
        CampaignCategory, on_delete=models.CASCADE, null=True, blank=True
    )
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "campaign_subcategories"

    def __str__(self):
        return f"{self.name}"


class CampaignEvent(AbstractBase):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "events"

    def __str__(self):
        return f"{self.name}"


class CampaignGroup(AbstractBase):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )
    organisation = models.ForeignKey(
       Organization , on_delete=models.CASCADE, null=True, blank=True
    )

    class Meta:
        db_table = "groups"

    def __str__(self):
        return f"{self.name}"


class Campaign(AbstractBase):
    campaign_name = models.CharField(max_length=255, unique=True)
    campaign_owner = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )
    campaign_category = models.ForeignKey(
        CampaignCategory, on_delete=models.CASCADE, null=True, blank=True
    )
    campaign_subcategory = models.ForeignKey(
        CampaignSubCategory, on_delete=models.CASCADE, null=True, blank=True
    )
    campaign_currency =  models.ForeignKey(
        Currency, on_delete=models.CASCADE, null=True, blank=True
    )
    campaign_event =  models.ForeignKey(
        CampaignEvent, on_delete=models.CASCADE, null=True, blank=True
    )
    campaign_story = models.TextField()
    campaign_start_date = models.DateField(null=True, blank=True)
    campaign_end_date = models.DateField(null=True, blank=True)
    campaign_target = models.IntegerField()
    thank_you_message = models.CharField(max_length=255)
    notification_frequency = models.CharField(max_length=255, null=True, blank=True)
    platform_fee_percentage =  models.CharField(max_length=100, null=True, blank=True)
    campaign_url =  models.CharField(max_length=100, unique=True)
    campaign_location =  models.CharField(max_length=255)
    campaign_images = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    campaign_images =  ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    socials = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    is_public =  models.BooleanField(default=False, null=True, blank=True)
    is_verified = models.BooleanField(default=False, null=True, blank=True)
    is_direct_funding = models.BooleanField(default=False, null=True, blank=True)
    is_editor_featured =models.BooleanField(default=False, null=True, blank=True)
    is_tax_benefit = models.BooleanField(default=False, null=True, blank=True)


    class Meta:
        db_table = "campaigns"

    def __str__(self):
        return f"{self.campaign_name}"


class CampaignUpdate(AbstractBase):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    campaign = models.ForeignKey(
       Campaign , on_delete=models.CASCADE, null=True, blank=True
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )

    class Meta:
        db_table = "campaign_updates"

    def __str__(self):
        return f"{self.name}"


class CampaignSupportDoc(AbstractBase):
    campaign = models.ForeignKey(
       Campaign , on_delete=models.CASCADE, null=True, blank=True
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )
    identification_document = models.CharField(max_length=255)
    other_documents = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )


    class Meta:
        db_table = "campaign_support_docs"

    def __str__(self):
        return f"{self.name}"


class ApprovalBuddy(AbstractBase):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null=True, blank=True
    )
    first_name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True)
    mobile_number = models.CharField(max_length=255, unique=True)
    is_active = models.BooleanField(default=False, null=True, blank=True)
    

    class Meta:
        db_table = "approval_buddies"

    def __str__(self):
        return f"{self.name}"


class CampaignMessage(AbstractBase):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null=True, blank=True
    )
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    message = models.TextField()


    class Meta:
        db_table = "campaign_messages"

    def __str__(self):
        return f"{self.name}"


class ReportCampaign(AbstractBase):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null=True, blank=True
    )
    type = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    contact = models.CharField(max_length=255, null=True, blank=True)
    
    description = models.CharField(max_length=255, null=True, blank=True)
    evidence = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    message = models.TextField()

    class Meta:
        db_table = "report_campaign"

    def __str__(self):
        return f"{self.name}"


class QuickLinks(AbstractBase):
    __tablename__ = "quick_links"
    type = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "quick_links"

    def __str__(self):
        return f"{self.name}"