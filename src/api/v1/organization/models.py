from django.contrib.postgres.fields.array import ArrayField
from django.db import models
from src.api.v1.user.models import User

from src.common.models import AbstractBase

class AccreditaionType(AbstractBase):
    __tablename__ = "accrediation_types"
    name =  models.CharField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=False, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "accrediation_types"

    def __str__(self):
        return f"{self.name}"



class OrganizationType(AbstractBase):
    name =  models.CharField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=False, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "organization_types"

    def __str__(self):
        return f"{self.name}"


class Organization(AbstractBase):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )
    legal_name = models.CharField(max_length=255, null=True, blank=True)
    other_name = models.CharField(max_length=255, null=True, blank=True)
    organization_type = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    office_type = models.CharField(max_length=255, null=True, blank=True)
    organization_location = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    organization_videos = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    organization_socials = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    s18_certificate_url = models.CharField(max_length=255, null=True, blank=True)
    legal_document_url = models.CharField(max_length=255, null=True, blank=True)
    support_docs_urls = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    help_required = models.BooleanField(default=False, null=True, blank=True)
    regular_number_of_donors = models.IntegerField()
    has_fulltime_staff = models.BooleanField(default=False, null=True, blank=True)
    has_crowdfunded_before = models.BooleanField(default=False, null=True, blank=True)
    has_used_events_before = models.BooleanField(default=False, null=True, blank=True)
    recieves_support = models.BooleanField(default=False, null=True, blank=True)
    crowd_funding_links = models.CharField(max_length=255, null=True, blank=True)
    support_partners = models.CharField(max_length=255, null=True, blank=True)


    class Meta:
        db_table = "organizations"

    def __str__(self):
        return f"{self.legal_name}"



class OrganizationUser(AbstractBase):
    __tablename__ = "organization_admin_roles"
    role = models.CharField(max_length=255, null=True, blank=True, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    organization = models.ForeignKey(
        Organization, on_delete=models.CASCADE, null=True, blank=True
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True
    )

    class Meta:
        db_table = "organization_users"

    def __str__(self):
        return f"{self.legal_name}"


class OrganizationTeamMember(AbstractBase):
    organization = models.ForeignKey(
        Organization, on_delete=models.CASCADE, null=True, blank=True
    )
    role = models.ForeignKey(
        OrganizationUser  , on_delete=models.CASCADE, null=True, blank=True
    )
    first_name = models.CharField(max_length=255, null=True, blank=True)
    surname = models.CharField(max_length=255, null=True, blank=True)
    email =  models.CharField(max_length=255, null=True, blank=True)
    mobile_number =  models.CharField(max_length=255, null=True, blank=True)
    position = models.CharField(max_length=255, null=True, blank=True)
    is_main_contact_person = models.BooleanField(default=False, null=True, blank=True)

    class Meta:
        db_table = "organization_team_members"

    def __str__(self):
        return f"{self.email}"


class OrganizationSector(AbstractBase):
    sector = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = "organization_sectors"

    def __str__(self):
        return f"{self.email}"


class OrganizationProfile(AbstractBase):
    sector = models.ForeignKey(
        OrganizationSector   , on_delete=models.CASCADE, null=True, blank=True
    )
    organization = models.ForeignKey(
        Organization  , on_delete=models.CASCADE, null=True, blank=True
    )
    keywords_description = models.TextField(null=True, blank=True)
    brief_description = models.TextField(null=True, blank=True)
    organization_logos = ArrayField(
        models.CharField(max_length=255, null=True, blank=True)
    )
    preset_tip = models.IntegerField(null=True, blank=True)


    class Meta:
        db_table = "organization_profiles"

    def __str__(self):
        return f"{self.organization}"