from django.db import models
from src.api.v1.location.models import Country
from src.common.models import AbstractBase

class Currency(AbstractBase):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, null=True, blank=True
    )

    class Meta:
        db_table = "currencies"

    def __str__(self):
        return f"{self.name}"


class CurrencyRates(AbstractBase):
    currency_from = models.ForeignKey(
        Currency, on_delete=models.CASCADE, null=True, blank=True, related_name='currency_from'
    )
    currency_to = models.ForeignKey(
        Currency, on_delete=models.CASCADE, null=True, blank=True, related_name='currency_to'
    )
    rate = models.IntegerField()
    date_from = models.DateField()


    class Meta:
        db_table = "currency_rates"

    def __str__(self):
        return f"{self.name}"
