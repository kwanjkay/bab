from django.db import models

from src.common.models import AbstractBase


class DonationStatus(AbstractBase):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    order = models.CharField(max_length=100)

    class Meta:
        db_table = "donation_status"


class TippingRate(AbstractBase):
    rate = models.CharField(max_length=50)
    date_from = models.DateField()
    class Meta:
        db_table = "tipping_rate"


class DonationType(AbstractBase):
    name = models.CharField(max_length=50)
    class Meta:
        db_table = "donation_type"


class Donation(AbstractBase):
    transaction_date = models.DateField()
    net_base_currency = models.CharField(max_length=100)
    gross_base_currency = models.CharField(max_length=50)
    net_amount = models.IntegerField()
    gross_amount = models.IntegerField()
    transaction_cost_amount = models.IntegerField()
    tipping_amount = models.IntegerField()
    description = models.CharField(max_length=200)
    transaction_ref = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    user = models.ForeignKey(
        'user.User',
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    doanation_type = models.ForeignKey(
        DonationType,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    donation_status = models.ForeignKey(
        DonationStatus,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    class Meta:
        db_table = "donation"


class RecurringDonation(AbstractBase):
    term_period = models.CharField(max_length=100)
    amount = models.IntegerField()
    deduction_start = models.DateField()
    deduction_end = models.DateField()
    deduction_date = models.CharField(max_length=100)
    active = models.BooleanField()
    class Meta:
        db_table = "recurring_donation"
