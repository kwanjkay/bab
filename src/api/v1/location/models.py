from django.db import models

from src.common.models import AbstractBase


class Country(AbstractBase):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    class Meta:
        db_table = "country"


class Town(AbstractBase):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    class Meta:
        db_table = "town"
