"""
Install project requirements.
"""

from setuptools import setup, find_packages

setup(
    name="babapi",
    version="0.1.0",
    description='BAB API',
    packages=find_packages(),
    include_package_data=True,
    scripts=["manage.py"],
    install_requires=[
        "Django==4.0.1",
        "djangorestframework==3.13.1",
        "python-decouple==3.5",
        "psycopg2-binary==2.9.3",
        "django-cors-middleware==1.5.0",
        "django-cors-headers==3.10.1",
        "dj-database-url==0.5.0",
        "gunicorn==20.1.0",
        "django-environ==0.8.1",
    ]
)
